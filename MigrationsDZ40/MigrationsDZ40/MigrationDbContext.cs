﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MigrationsDZ40.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrationsDZ40
{
    public class MigrationDbContext:DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var bilder = new ConfigurationBuilder();
            bilder.SetBasePath(@"D:\vs_comerc\migrationsdz40\MigrationsDZ40\MigrationsDZ40\");
            bilder.AddJsonFile("appsetings.json");
            var config = bilder.Build();
            var connectionString = config.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString);
        }
    }
}
