﻿using MigrationsDZ40.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MigrationsDZ40.Models
{
    public class Post : IEntity<int>
    {
        public int Id { get; set; }
        [ForeignKey("User")]
        public int UserId { get; set; }
        public string Comment { get; set; }
        public virtual User User { get; set; }
    }
}
