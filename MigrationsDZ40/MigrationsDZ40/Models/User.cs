﻿using MigrationsDZ40.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrationsDZ40.Models
{
    public class User: IEntity<int>
    {
        public User()
        {
            AccountCreationDate = DateTime.Now;
        }

        public  int Id {get;set;}
        public string Login {get;set;}
        public string FirstName { get;set;}
        public string LasstName {get;set;}
        public DateTime DateofBirth {get;set;}
        public DateTime AccountCreationDate { get; set; }

    }
}
