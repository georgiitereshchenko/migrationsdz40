﻿using Microsoft.EntityFrameworkCore;
using MigrationsDZ40.Models;
using System;
using System.Linq;

namespace MigrationsDZ40
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Привет что хотите сделать?");
            Console.WriteLine("Создать нового пользователя :1 \n" +
                "Создать новый Пост 2 \n" +
                "Вывести всех пользователей 3\n" +
                "Вывести все посты 4");
            int knifeSwitch = 0;

            Console.Write(">");
            int.TryParse(Console.ReadLine(), out knifeSwitch);

            switch (knifeSwitch)
            {
                case 1:
                    AddUser();
                    break;
                case 2:
                    AddPost();
                    break;
                case 3:
                    PrintAllUser();
                    break;
                case 4:
                    PrintAllPost();
                    break;
                default:
                    Console.WriteLine("Я не знаю такой каманды");
                    break;
            }
                
        }


        public static void AddUser()
        {
            using (var db = new MigrationDbContext())
            {
                var newUser = new User();
                Console.WriteLine("Ведите Логин");
                Console.Write(">");
                newUser.Login = Console.ReadLine();
                 
                Console.WriteLine("Ведите Имя");
                Console.Write(">");
                newUser.FirstName = Console.ReadLine();
                
                Console.WriteLine("Ведите Фамилию");
                Console.Write(">");
                newUser.LasstName = Console.ReadLine();
                
                Console.WriteLine("Ведите Дату рождения");
                Console.Write(">");
                newUser.DateofBirth = DateTime.Parse( Console.ReadLine());

                db.Users.Add(newUser);
                db.SaveChanges();
            }
        }
        public static void PrintAllUser()
        {
            using (var db = new MigrationDbContext())
            {
                var allUser = db.Users.ToList();
                foreach (var user in allUser)
                {
                    Console.WriteLine($"Логин: {user.Login}");
                    Console.WriteLine($"Имя: {user.FirstName}");
                    Console.WriteLine($"Фамилию: {user.FirstName}");
                    Console.WriteLine($"Дата рождения: {user.DateofBirth}");
                    Console.WriteLine($"Создания акаунта: {user.AccountCreationDate}");
                }

            }
        }

        public static void AddPost() 
        {
            using (var db = new MigrationDbContext())
            {
                var newPost = new Post();
                Console.WriteLine("Ведите Логин пользователя который оставит коментарий");
                Console.Write(">");
                string Login = Console.ReadLine();

                var User = db.Users.FirstOrDefault(x => x.Login == Login);

                if (User == null)
                {
                    Console.WriteLine("Пользователь не найден досведания");
                    return;
                }

                newPost.UserId = User.Id;

                Console.WriteLine("Ведите Коментарий");
                Console.Write(">");
                newPost.Comment = Console.ReadLine();

                

                db.Posts.Add(newPost);
                db.SaveChanges();
            }
        }
        public static void PrintAllPost()
        {
            using (var db = new MigrationDbContext())
            {
                var allPost = db.Posts.Include(x => x.User).ToList();
                foreach (var x in allPost)
                {
                    Console.WriteLine($"Логин Пользователя: {x.User.Login}");
                    Console.WriteLine($"Коментарий: {x.Comment}");
                   
                }

            }
        }
        
    }
}
